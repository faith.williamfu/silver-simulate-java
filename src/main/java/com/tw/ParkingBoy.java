package com.tw;

public class ParkingBoy {
    private final ParkingLot parkingLot;
    private String lastErrorMessage;

    public ParkingBoy(ParkingLot parkingLot) {
        this.parkingLot = parkingLot;
    }

    public ParkingTicket park(Car car) {
        // TODO: Implement the method according to test
        // <-start-
        ParkingResult ticket = parkingLot.park(car);
        if (!ticket.isSuccess()) {
            lastErrorMessage = ticket.getMessage();
        }
        return ticket.getTicket();
        // ---end->
    }

    public Car fetch(ParkingTicket ticket) {
        // TODO: Implement the method according to test
        // <-start-
        FetchingResult car = parkingLot.fetch(ticket);
        if (!car.isSuccess()) {
            lastErrorMessage = car.getMessage();
        }
        return car.getCar();
        // ---end->
    }

    public String getLastErrorMessage() {
        return lastErrorMessage;
    }
}
